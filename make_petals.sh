#!/bin/sh

IN_ROW=15

DELAY_BETWEEN=0.2
DEG_BETWEEN=24

SUBUNITS="4.9 3.5 2.5 1.5 1"
SUBUNITS_N=$(wc -w <<EOF
$SUBUNITS
EOF
	     )
END_DEG=360
ANIMATION_DURATION=10 # must match that in chryzantem.html.template
ANIMATION_DELAY=$(bc <<EOF
2 + $SUBUNITS_N * $IN_ROW * $DELAY_BETWEEN
EOF
		  )

print_petal() {
    cat <<EOF
      <div class="petal">
	<div style="--subunit: $SUBUNIT; --end-deg: -${END_DEG}deg; animation-delay: ${ANIMATION_DELAY}s;">
	  <img src="./petal.svg" alt="flower petal" draggable="false">
	</div>
      </div>
EOF
}

I=0
for SUBUNIT in $SUBUNITS; do
    I=$((I +  1))

    if [ $I -eq $SUBUNITS_N ]; then
	bc >_inner_row_delay <<EOF
$ANIMATION_DELAY + 10
EOF
    fi

    if [ $I -eq 1 ]; then
	bc >_flower_delay <<EOF
$ANIMATION_DELAY + 10 - $DELAY_BETWEEN
EOF
    fi

    for _ in $(seq $IN_ROW); do
	print_petal
	END_DEG=$((END_DEG + DEG_BETWEEN))
	ANIMATION_DELAY=$(bc <<EOF
$ANIMATION_DELAY - $DELAY_BETWEEN
EOF
		   )
    done

    END_DEG=$((END_DEG + DEG_BETWEEN / 2))
done
